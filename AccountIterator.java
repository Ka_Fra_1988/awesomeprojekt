
/** 
* AccountIterator.java
* Stellt einen Konto Iterator dar
* @author Katharian Franz
* @version 1.1
*/
import java.util.ArrayList;

//This class is responsible the Iterator design pattern
/**
 * Dies ist die Dokumentation der Klasse AccountIterator (kontoIterator) * This class is responsible the Iterator design patter
 * Die Kasse AccountIterator ist zust�ndig f�r das Iterator Design Pattern (Entwurfsmuster)
 * Sie Iimplementiert das Interface Iterator welches f�r die Iterationsmuster (Wiederholungsmuster) zust�ndig ist
 * Sie beihaltet eine bereits bestehende ArrayListe welche in der Klasse verwendet wird
 */
public class AccountIterator implements Iterator {
    ArrayList<Account> accounts;
    
    public AccountIterator(ArrayList<Account> accounts2) {
		this.accounts = accounts2;
	} //Ende der Klasse AccountIterator (kontoIterator)
    
    
    /** 
    	* Die Funktion hasNext (hatAlsNaechstes) gibt true zur�ck sobald ? This function returns true if the ArrayList has a space next to the current one
    	* Ist position gr��er gleich accounts.size gebe false zur�ck
    	* Asonsten gebe true zur�ck		
    	*/
	public boolean hasNext(int position) {
		if (position >= accounts.size()) {
			return false;
		} else {
			return true;
		}
	}
	

	@Override //This function iterates to the next position in the ArrayList.
	public Object next(int position) {
		Account AccountItem = accounts.get(position);
		return AccountItem;
	}


	@Override
	//This function checks when the position is at 0, and prevents the user from going back even further. 
	public boolean hasPrev(int position) {
		if(position == 0)
		return false;
		else
			return true;
	}
	



	

}
